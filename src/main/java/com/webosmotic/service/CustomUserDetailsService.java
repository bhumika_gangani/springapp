package com.webosmotic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.webosmotic.exception.ResourceNotFoundException;
import com.webosmotic.model.User;
import com.webosmotic.repository.UserRepository;
import com.webosmotic.security.UserPrincipal;


@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	//@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username).orElseThrow(
				() -> new UsernameNotFoundException("User not found with username or email : " + username));

		return UserPrincipal.create(user);
	}

	//@Transactional
	public UserDetails loadUserById(String id) {
		User user = userRepository.findById(Long.parseLong(id))
				.orElseThrow(() -> new ResourceNotFoundException("User application ", "Id", id));
		return UserPrincipal.create(user);
	}

}
