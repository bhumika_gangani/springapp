package com.webosmotic.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.webosmotic.model.User;
import com.webosmotic.payload.UserDTO;
import com.webosmotic.payload.ResponseWrapperDTO;
import com.webosmotic.repository.UserRepository;
import com.webosmotic.utils.VariablesUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public ResponseWrapperDTO addUser(User user) throws Exception {
		LOGGER.info("User :: {}" + user);
		LOGGER.info("Check username already exist or not!");
		Optional<User> userByUserName = userRepository.findByUsername(user.getUsername());
		if (userByUserName.isPresent()) {
			return new ResponseWrapperDTO(HttpStatus.CONFLICT, VariablesUtils.DATA_ALREADY_EXIST,
					"Username " + VariablesUtils.DATA_ALREADY_EXIST, null);
		}
		LOGGER.info("Check email exist or not!");
		Optional<User> userByUserEmail = userRepository.findByEmail(user.getEmail());
		if (userByUserEmail.isPresent()) {
			return new ResponseWrapperDTO(HttpStatus.CONFLICT, VariablesUtils.DATA_ALREADY_EXIST,
					"User email " + VariablesUtils.DATA_ALREADY_EXIST, null);
		}

		String passwd = bcryptEncoder.encode(user.getPassword());
		user.setPassword(passwd);
		userRepository.save(user);
		return new ResponseWrapperDTO(HttpStatus.OK, "User " + VariablesUtils.SAVE_DATA_SUCCESSFULLY, null, null);
	}

	@Override
	@Transactional
	public ResponseWrapperDTO updateUser(User user, String userId) throws Exception {

		LOGGER.info("User :: {}" + user);
		Optional<User> userfromdb = userRepository.findById(Long.parseLong(userId));
		if (!userfromdb.isPresent()) {
			return new ResponseWrapperDTO(HttpStatus.CONFLICT, VariablesUtils.RECORD_NOT_FOUND_IN_DB,
					VariablesUtils.RECORD_NOT_FOUND_IN_DB, null);
		}
		LOGGER.info("Check username already exist or not!");
		Optional<User> userByUserName = userRepository.findByUsername(user.getUsername());
		if (userByUserName.isPresent() && !userId.equals(userByUserName.get().getId().toString())) {
			return new ResponseWrapperDTO(HttpStatus.CONFLICT, VariablesUtils.DATA_ALREADY_EXIST,
					"Username " + VariablesUtils.DATA_ALREADY_EXIST, null);
		}
		userfromdb.get().setFirstname(user.getFirstname());
		userfromdb.get().setLastname(user.getLastname());
		userfromdb.get().setUsername(user.getUsername());

		userRepository.save(userfromdb.get());
		return new ResponseWrapperDTO(HttpStatus.OK, "User " + VariablesUtils.UPDATE_DATA_SUCCESSFULLY, null, null);
	}

	@Override
	public ResponseWrapperDTO deleteUserById(String userId) throws Exception {
		Optional<User> userfromdb = userRepository.findById(Long.parseLong(userId));
		if (!userfromdb.isPresent()) {
			return new ResponseWrapperDTO(HttpStatus.CONFLICT, VariablesUtils.RECORD_NOT_FOUND_IN_DB,
					VariablesUtils.RECORD_NOT_FOUND_IN_DB, null);
		}
		userRepository.delete(userfromdb.get());
		return new ResponseWrapperDTO(HttpStatus.OK, "User " + VariablesUtils.DELETE_DATA_SUCCESSFULLY, null, null);
	}

	@Override
	public ResponseWrapperDTO getUserById(String userId) throws Exception {
		Optional<User> userfromdb = userRepository.findById(Long.parseLong(userId));
		if (!userfromdb.isPresent()) {
			return new ResponseWrapperDTO(HttpStatus.CONFLICT, VariablesUtils.RECORD_NOT_FOUND_IN_DB,
					VariablesUtils.RECORD_NOT_FOUND_IN_DB, null);
		}

		return new ResponseWrapperDTO(HttpStatus.OK, VariablesUtils.GET_DATA_SUCCESSFULLY, null, userfromdb);
	}

	@Override
	public ResponseWrapperDTO getUser() {
		return new ResponseWrapperDTO(HttpStatus.OK, VariablesUtils.GET_DATA_SUCCESSFULLY, null,
				userRepository.findAll());
	}


}
