package com.webosmotic.service;

import com.webosmotic.model.User;
import com.webosmotic.payload.ResponseWrapperDTO;

public interface UserService {

	/***
	 * Register new user and add new user
	 * @author Webosmotic 
	 * @param user Object of user
	 * @exception User already exist
	 */
	ResponseWrapperDTO addUser(User user) throws Exception;
	
	
	/***
	 * Updates exist user by Id
	 * 
	 * @author Webosmotic 
	 * @param userId 
	 * @exception User Not found
	 *  
	 */
	ResponseWrapperDTO updateUser(User body, String userId) throws Exception;

	/***
	 * Delete user based on user Id
	 * @author Webosmotic 
	 * @param userId
	 * @exception User Not found
	 */
	ResponseWrapperDTO deleteUserById(String userId) throws Exception;
	
	/***
	 * Get user details by Id 
	 * @author Webosmotic 
	 * @param userId
	 * @exception User Not found
	 */
	ResponseWrapperDTO getUserById(String userId) throws Exception;
	
	/***
	 * Get All exist user in database
	 * @author Webosmotic 
	 */
	ResponseWrapperDTO getUser();
	
}
