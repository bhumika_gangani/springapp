package com.webosmotic.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webosmotic.model.User;
import com.webosmotic.payload.ResponseWrapperDTO;
import com.webosmotic.service.UserService;
import com.webosmotic.utils.MethodsUtils;
import com.webosmotic.utils.VariablesUtils;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/addUser")
	public ResponseEntity<ResponseWrapperDTO> addUser(@Valid @RequestBody User user, HttpServletRequest request) {
		try {
			return new ResponseEntity<ResponseWrapperDTO>(userService.addUser(user), HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseWrapperDTO>(
					new ResponseWrapperDTO(HttpStatus.OK, e.getMessage(), null, null), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/updateUser/{userId}")
	public ResponseEntity<ResponseWrapperDTO> updateUser(@RequestBody User user, @PathVariable("userId") String userId,
			HttpServletRequest request) {
		try {
			return new ResponseEntity<ResponseWrapperDTO>(userService.updateUser(user, userId), HttpStatus.BAD_REQUEST);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseWrapperDTO>(
					new ResponseWrapperDTO(HttpStatus.OK, e.getMessage(), null, null), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/deleteUserById/{userId}")
	public ResponseEntity<ResponseWrapperDTO> deleteUserById(@PathVariable("userId") String userId,
			HttpServletRequest request) {
		try {
			if (MethodsUtils.isObjectNullOrEmpty(userId)) {
				return new ResponseEntity<ResponseWrapperDTO>(
						new ResponseWrapperDTO(HttpStatus.OK, VariablesUtils.INCOMPLETE_REQUEST_DATA, null, null),
						HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<ResponseWrapperDTO>(userService.deleteUserById(userId), HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseWrapperDTO>(
					new ResponseWrapperDTO(HttpStatus.OK, e.getMessage(), null, null), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/getUserById/{userId}")
	public ResponseEntity<ResponseWrapperDTO> getUserById(@PathVariable("userId") String userId,
			HttpServletRequest request) {
		try {
			if (MethodsUtils.isObjectNullOrEmpty(userId)) {
				return new ResponseEntity<ResponseWrapperDTO>(
						new ResponseWrapperDTO(HttpStatus.OK, VariablesUtils.INCOMPLETE_REQUEST_DATA, null, null),
						HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<ResponseWrapperDTO>(userService.getUserById(userId), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseWrapperDTO>(
					new ResponseWrapperDTO(HttpStatus.OK, e.getMessage(), null, null), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/getUser")
	public ResponseEntity<ResponseWrapperDTO> getUser(@RequestParam(value = "userId", required = false) Long userId,
			HttpServletRequest request) {
		try {
			
			return new ResponseEntity<ResponseWrapperDTO>(userService.getUser(), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ResponseWrapperDTO>(
					new ResponseWrapperDTO(HttpStatus.OK, e.getMessage(), null, null), HttpStatus.BAD_REQUEST);
		}
	}
}
