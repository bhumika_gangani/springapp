package com.webosmotic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication(scanBasePackages = { "com.webosmotic"})
public class SpringApp extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SpringApp.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringApp.class);
	}

	@GetMapping("/")
	public String testApplication() {
		return "Welcome to Webosmotic !";
	}
}
