package com.webosmotic.payload;

public class RequestWrapperDTO {

	private Object jsonOfObject;

	public Object getJsonOfObject() {
		return jsonOfObject;
	}

	public void setJsonOfObject(Object jsonOfObject) {
		this.jsonOfObject = jsonOfObject;
	}

}
